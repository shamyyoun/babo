package com.xapo.xapotask.data.apis

interface ApiManager {
    fun <T> createApi(apiClass: Class<T>): T
}