package no.babo.android.data.repositories

import android.content.Context
import no.babo.android.data.prefs.PrefsManagerImpl
import no.babo.android.utils.Const
import no.babo.android.utils.InjectionUtils

/**
 * Created by Shamyyoun on 8/27/16.
 */
class SettingsRepository(context: Context) {
    private val prefsManager: PrefsManagerImpl = InjectionUtils.providePrefsManager(context)

    fun updateLanguage(language: String) {
        prefsManager.save(language, Const.KEY_LANGUAGE)
    }

    fun getLanguage(): String? {
        return prefsManager.load(Const.KEY_LANGUAGE)
    }

    companion object {
        private var activeUserRepository: SettingsRepository? = null

        fun instance(context: Context): SettingsRepository {
            if (activeUserRepository == null) {
                activeUserRepository = SettingsRepository(context)
            }

            return activeUserRepository!!
        }
    }
}
