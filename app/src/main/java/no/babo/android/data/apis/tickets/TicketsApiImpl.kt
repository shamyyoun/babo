package no.babo.android.data.apis.tickets

import no.babo.android.data.apis.interceptors.AuthorizationInterceptor
import no.babo.android.data.apis.models.CheckinResponse
import no.babo.android.data.apis.models.EventsResponse
import no.babo.android.data.apis.models.TicketsResponse
import no.babo.android.utils.InjectionUtils
import okhttp3.Interceptor
import retrofit2.Call

class TicketsApiImpl(token: String) {
    private val ticketsApi: TicketsApi

    init {
        val interceptors = ArrayList<Interceptor>()
        interceptors.add(AuthorizationInterceptor(token))
        val apiManagerImpl = InjectionUtils.provideApiManager(interceptors)
        ticketsApi = apiManagerImpl.createApi(TicketsApi::class.java)
    }

    fun tickets(token: String, eventId: Int): Call<TicketsResponse> {
        return ticketsApi.tickets("Bearer $token", eventId)
    }

    fun checkin(token: String, ticketId: String, securityCode: String): Call<CheckinResponse> {
        return ticketsApi.checkin("Bearer $token", ticketId, securityCode)
    }

    fun checkin(token: String, provider: String, attendeeId: Int, eventId: Int): Call<CheckinResponse> {
        return ticketsApi.checkin("Bearer $token", provider, attendeeId, eventId)
    }

    fun myEvents(token: String): Call<EventsResponse> {
        return ticketsApi.myEvents("Bearer $token")
    }
}