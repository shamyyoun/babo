package no.babo.android.data.models

import com.google.gson.annotations.SerializedName

open class User(
        @SerializedName("user_email")
        val email: String = "",
        @SerializedName("user_display_name")
        val displayName: String = "",
        @SerializedName("token")
        val token: String = "",
        @SerializedName("active_events")
        val events: List<Event> = ArrayList(),
        var activeEventId: Int = 0
)