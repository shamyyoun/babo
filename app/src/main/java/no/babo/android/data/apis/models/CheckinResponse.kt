package no.babo.android.data.apis.models

import com.google.gson.annotations.SerializedName

data class CheckinResponse(@SerializedName("toggled")
                           var toggled: Boolean = false,
                           @SerializedName("checkin_status")
                           val checkinStatus: Boolean = false,
                           @SerializedName("msg")
                           val msg: String = "")