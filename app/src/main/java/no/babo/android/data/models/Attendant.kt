package no.babo.android.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Attendant(
        @SerializedName("title")
        val title: String = "",
        @SerializedName("ticket_id")
        val ticketId: Int = 0,
        @SerializedName("id")
        val id: Int = 0,
        @SerializedName("checked_in")
        var checkedIn: Boolean = false,
        @SerializedName("paid_amount")
        val paidAmount: Int = 0,
        @SerializedName("security_code")
        val securityCode: String = "",
        @SerializedName("provider")
        val provider: String = "",
        @SerializedName("unique_id")
        val uniqueId: String = "",
        @SerializedName("post_id")
        val eventId: Int = 0,
        @SerializedName("checked_timestamp")
        var checkInDate: Long = 0) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readString(),


            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readLong())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeInt(ticketId)
        parcel.writeInt(id)
        parcel.writeByte(if (checkedIn) 1 else 0)
        parcel.writeInt(paidAmount)
        parcel.writeString(securityCode)
        parcel.writeString(provider)
        parcel.writeString(uniqueId)
        parcel.writeInt(eventId)
        parcel.writeLong(checkInDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Attendant> {
        override fun createFromParcel(parcel: Parcel): Attendant {
            return Attendant(parcel)
        }

        override fun newArray(size: Int): Array<Attendant?> {
            return arrayOfNulls(size)
        }
    }
}