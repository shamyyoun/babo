package no.babo.android.data.apis.models

import com.google.gson.annotations.SerializedName
import no.babo.android.data.models.Ticket

data class TicketsResponse(@SerializedName("total")
                           val total: Int = 0,
                           @SerializedName("tickets")
                           val tickets: List<Ticket>?,
                           @SerializedName("total_pages")
                           val totalPages: Int = 0)