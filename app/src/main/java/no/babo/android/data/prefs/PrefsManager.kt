package no.babo.android.data.prefs

interface PrefsManager {

    fun <T> save(obj: T, key: String)

    fun <T> load(key: String, cls: Class<*> = String::class.java): T?

    fun remove(key: String)
}