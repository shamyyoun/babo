package no.babo.android.data.repositories

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import no.babo.android.data.apis.models.CheckinResponse
import no.babo.android.data.apis.models.EventsResponse
import no.babo.android.data.apis.models.TicketsResponse
import no.babo.android.data.apis.tickets.TicketsApiImpl
import no.babo.android.data.models.Event
import no.babo.android.data.models.Ticket
import no.babo.android.utils.InjectionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TicketsRepository(private val token: String) {
    private val ticketsApi: TicketsApiImpl = InjectionUtils.provideTicketsApi(token)

    fun tickets(eventId: Int): LiveData<List<Ticket>> {
        val liveData = MutableLiveData<List<Ticket>>()
        val call = ticketsApi.tickets(token, eventId)

        call.enqueue(object : Callback<TicketsResponse> {
            override fun onFailure(call: Call<TicketsResponse>, t: Throwable) {
                liveData.value = null
            }

            override fun onResponse(call: Call<TicketsResponse>, response: Response<TicketsResponse>) {
                liveData.value = response.body()?.tickets
            }
        })

        return liveData
    }

    fun checkin(securityCode: String, ticketId: String): LiveData<Boolean> {
        val liveData = MutableLiveData<Boolean>()
        val call = ticketsApi.checkin(token, ticketId, securityCode)

        call.enqueue(object : Callback<CheckinResponse> {
            override fun onFailure(call: Call<CheckinResponse>, t: Throwable) {
                liveData.value = false
            }

            override fun onResponse(call: Call<CheckinResponse>, response: Response<CheckinResponse>) {
                liveData.value = when {
                    response.code() == 200 || response.code() == 201 -> true
                    response.body().toString().toLowerCase().contains("already checked") -> true
                    else -> false
                }
            }
        })

        return liveData
    }

    fun checkin(provider: String, attendeeId: Int, event: Int): LiveData<Boolean> {
        val liveData = MutableLiveData<Boolean>()
        val call = ticketsApi.checkin(token, provider, attendeeId, event)

        call.enqueue(object : Callback<CheckinResponse> {
            override fun onFailure(call: Call<CheckinResponse>, t: Throwable) {
                liveData.value = false
            }

            override fun onResponse(call: Call<CheckinResponse>, response: Response<CheckinResponse>) {
                liveData.value = response.code() == 200
            }
        })

        return liveData
    }

    fun myEvents(): LiveData<List<Event>> {
        val liveData = MutableLiveData<List<Event>>()
        val call = ticketsApi.myEvents(token)

        call.enqueue(object : Callback<EventsResponse> {
            override fun onFailure(call: Call<EventsResponse>, t: Throwable) {
                liveData.value = null
            }

            override fun onResponse(call: Call<EventsResponse>, response: Response<EventsResponse>) {
                liveData.value = response.body()?.events
            }
        })

        return liveData
    }
}