package no.babo.android.data.apis.authentication

import no.babo.android.data.models.User
import no.babo.android.utils.InjectionUtils
import retrofit2.Call

class AuthenticationApiImpl {
    private val authenticationApi: AuthenticationApi

    init {
        val apiManagerImpl = InjectionUtils.provideApiManager()
        authenticationApi = apiManagerImpl.createApi(AuthenticationApi::class.java)
    }

    fun login(username: String, password: String): Call<User> {
        return authenticationApi.login(username, password)
    }

    fun login(apiKey: String): Call<User> {
        return authenticationApi.login(apiKey)
    }
}