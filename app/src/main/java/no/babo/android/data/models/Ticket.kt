package no.babo.android.data.models

import com.google.gson.annotations.SerializedName

data class Ticket(@SerializedName("date")
                  val date: String = "",
                  @SerializedName("description")
                  val description: String = "",
                  @SerializedName("title")
                  val title: String = "",
                  @SerializedName("capacity")
                  val capacity: String = "",
                  @SerializedName("modified_utc")
                  val modifiedUtc: String = "",
                  @SerializedName("global_id_lineage")
                  val globalIdLineage: List<String>?,
                  @SerializedName("provider")
                  val provider: String = "",
                  @SerializedName("requires_attendee_information")
                  val requiresAttendeeInformation: Boolean = false,
                  @SerializedName("modified")
                  val modified: String = "",
                  @SerializedName("id")
                  val id: Int = 0,
                  @SerializedName("available_until")
                  val availableUntil: String = "",
                  @SerializedName("image")
                  val image: Boolean = false,
                  @SerializedName("cost")
                  val cost: String = "",
                  @SerializedName("supports_attendee_information")
                  val supportsAttendeeInformation: Boolean = false,
                  @SerializedName("author")
                  val author: String = "",
                  @SerializedName("attendees")
                  val attendees: List<Attendant>,
                  @SerializedName("global_id")
                  val globalId: String = "",
                  @SerializedName("rest_url")
                  val restUrl: String = "",
                  @SerializedName("available_from")
                  val availableFrom: String = "",
                  @SerializedName("is_available")
                  val isAvailable: Boolean = false,
                  @SerializedName("date_utc")
                  val dateUtc: String = "",
                  @SerializedName("post_id")
                  val postId: Int = 0,
                  @SerializedName("status")
                  val status: String = "")