package no.babo.android.data.apis.models

import com.google.gson.annotations.SerializedName
import no.babo.android.data.models.Event

data class EventsResponse(@SerializedName("active_events")
                          val events: List<Event>?)