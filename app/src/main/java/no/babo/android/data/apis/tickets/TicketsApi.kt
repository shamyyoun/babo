package no.babo.android.data.apis.tickets

import no.babo.android.data.apis.models.CheckinResponse
import no.babo.android.data.apis.models.EventsResponse
import no.babo.android.data.apis.models.TicketsResponse
import no.babo.android.utils.Const
import retrofit2.Call
import retrofit2.http.*

interface TicketsApi {
    @GET("tribe/tickets/v1/tickets/?per_page=2147483647")
    fun tickets(@Header("Authorization") token: String, @Query("include_post") eventId: Int): Call<TicketsResponse>

    @GET("tribe/tickets/v1/qr?api_key=${Const.API_KEY}")
    fun checkin(@Header("Authorization") token: String, @Query("ticket_id") ticketId: String, @Query("security_code") securityCode: String): Call<CheckinResponse>

    @FormUrlEncoded
    @POST("tribe/tickets/v1/toggle_check_in")
    fun checkin(@Header("Authorization") token: String, @Field("provider") provider: String, @Field("attendee_id") attendeeId: Int, @Field("event_id") eventId: Int): Call<CheckinResponse>

    @GET("tribe/tickets/v1/my_events")
    fun myEvents(@Header("Authorization") token: String): Call<EventsResponse>
}