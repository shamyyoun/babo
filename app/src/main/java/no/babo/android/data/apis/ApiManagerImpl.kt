package com.xapo.xapotask.data.apis

import no.babo.android.utils.Const
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiManagerImpl : ApiManager {
    private lateinit var retrofit: Retrofit
    private val retrofitBuilder = Retrofit.Builder()
    private val httpOk = OkHttpClient.Builder()

    constructor(interceptors: List<Interceptor>) {
        retrofitBuilder.baseUrl(Const.END_POINT)
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create())

        // add basic interceptors
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        httpOk.addInterceptor(interceptor)

        // add user defined interceptors
        for (interceptor in interceptors) {
            httpOk.addInterceptor(interceptor)
        }

        // set client
        retrofitBuilder.client(httpOk.build())
    }

    override fun <T> createApi(apiClass: Class<T>): T {
        retrofit = retrofitBuilder.build()
        return retrofit.create(apiClass)
    }

    companion object {
        private var apiManagerImpl: ApiManagerImpl? = null

        fun instance(interceptors: List<Interceptor>): ApiManagerImpl {
            if (apiManagerImpl == null) {
                apiManagerImpl = ApiManagerImpl(interceptors)
            }

            return apiManagerImpl!!
        }
    }
}