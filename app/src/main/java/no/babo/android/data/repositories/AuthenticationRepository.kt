package no.babo.android.data.repositories

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import no.babo.android.data.models.User
import no.babo.android.utils.InjectionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthenticationRepository {
    private val authenticationApi = InjectionUtils.provideAuthenticationApi()

    fun login(username: String = "", password: String = "", apiKey: String = ""): LiveData<User> {
        val liveData = MutableLiveData<User>()
        val call = when (apiKey) {
            "" -> authenticationApi.login(username, password)
            else -> authenticationApi.login(apiKey)
        }

        call.enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                liveData.value = null
            }

            override fun onResponse(call: Call<User>, response: Response<User>) {
                liveData.value = response.body()
            }
        })

        return liveData
    }
}