package no.babo.android.data.apis.authentication

import no.babo.android.data.models.User
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthenticationApi {
    @FormUrlEncoded
    @POST("jwt-auth/v1/token")
    fun login(@Field("username") username: String, @Field("password") password: String): Call<User>

    @FormUrlEncoded
    @POST("jwt-auth/v1/token")
    fun login(@Field("login_key") loginKey: String): Call<User>
}