package no.babo.android.data.enums

enum class Language(val lang: String) {
    NORWEGIAN("nb"), ENGLISH("en")
}