package no.babo.android.data.repositories

import android.content.Context
import no.babo.android.data.models.User
import no.babo.android.data.prefs.PrefsManagerImpl
import no.babo.android.utils.Const
import no.babo.android.utils.InjectionUtils

/**
 * Created by Shamyyoun on 8/27/16.
 */
class ActiveUserRepository(context: Context) {
    private val prefsManager: PrefsManagerImpl = InjectionUtils.providePrefsManager(context)
    private var user: User? = null

    fun hasActiveUser() = getUser() != null

    fun getUser(): User? {
        if (user == null) {
            user = prefsManager.load(Const.KEY_ACTIVE_USER, User::class.java)
        }
        return user
    }

    fun updateUser(user: User) {
        this.user = user
        prefsManager.save(user, Const.KEY_ACTIVE_USER)
    }

    fun removeUser() {
        prefsManager.remove(Const.KEY_ACTIVE_USER)
        user = null
    }

    companion object {
        private var activeUserRepository: ActiveUserRepository? = null

        fun instance(context: Context): ActiveUserRepository {
            if (activeUserRepository == null) {
                activeUserRepository = ActiveUserRepository(context)
            }

            return activeUserRepository!!
        }
    }
}
