package no.babo.android.utils

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import com.xapo.xapotask.data.apis.ApiManagerImpl
import no.babo.android.data.apis.authentication.AuthenticationApiImpl
import no.babo.android.data.apis.tickets.TicketsApiImpl
import no.babo.android.data.prefs.PrefsManagerImpl
import no.babo.android.data.repositories.ActiveUserRepository
import no.babo.android.data.repositories.AuthenticationRepository
import no.babo.android.data.repositories.SettingsRepository
import no.babo.android.data.repositories.TicketsRepository
import no.babo.android.ui.base.BaseActivity
import no.babo.android.ui.checkin.CheckinViewModel
import no.babo.android.ui.eventdetails.attendees.AttendeesViewModel
import no.babo.android.ui.eventdetails.details.EventDetailsDataHolder
import no.babo.android.ui.eventdetails.details.EventDetailsViewModel
import no.babo.android.ui.events.EventsViewModel
import no.babo.android.ui.login.LoginViewModel
import no.babo.android.ui.settings.SettingsViewModel
import no.babo.android.ui.shared.ViewModelCustomProvider
import no.babo.android.ui.splash.SplashViewModel
import okhttp3.Interceptor

object InjectionUtils {
    fun providePrefsManager(context: Context) = PrefsManagerImpl(context)

    fun provideApiManager(interceptors: List<Interceptor> = ArrayList()) = ApiManagerImpl.instance(interceptors)

    fun provideAuthenticationApi() = AuthenticationApiImpl()

    fun provideAuthenticationRepository() = AuthenticationRepository()

    fun provideActiveUserRepository(context: Context) = ActiveUserRepository.instance(context)

    fun provideSplashViewModel(activity: BaseActivity): SplashViewModel {
        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(SplashViewModel(activity.application, provideAuthenticationRepository())))
                .get(SplashViewModel::class.java)
    }

    fun provideLoginViewModel(activity: BaseActivity): LoginViewModel {
        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(LoginViewModel(activity.application, provideAuthenticationRepository())))
                .get(LoginViewModel::class.java)
    }

    fun provideEventsViewModel(activity: BaseActivity): EventsViewModel {
        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(EventsViewModel(activity.application)))
                .get(EventsViewModel::class.java)
    }

    fun provideTicketsApi(token: String) = TicketsApiImpl(token)

    fun provideTicketsRepository(token: String) = TicketsRepository(token)

    fun provideEventDetailsViewModel(activity: BaseActivity): EventDetailsViewModel {
        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(EventDetailsViewModel(activity.application)))
                .get(EventDetailsViewModel::class.java)
    }

    fun provideCheckinViewModel(activity: BaseActivity): CheckinViewModel {
        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(CheckinViewModel(activity.application)))
                .get(CheckinViewModel::class.java)
    }

    fun provideSettingsViewModel(activity: BaseActivity): SettingsViewModel {
        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(SettingsViewModel(activity.application)))
                .get(SettingsViewModel::class.java)
    }

    fun provideSettingsRepository(context: Context) = SettingsRepository.instance(context)

    fun provideAttendeesViewModel(activity: BaseActivity, eventDetails: EventDetailsDataHolder,
                                  isSoldScreen: Boolean): AttendeesViewModel {
        return ViewModelProviders.of(
                activity,
                ViewModelCustomProvider(AttendeesViewModel(activity.application, eventDetails, isSoldScreen)))
                .get(AttendeesViewModel::class.java)
    }
}