package no.babo.android.utils

object Const {
    // app constants
//    const val END_POINT = "http://babo.stagings.teamyea.com/wp-json/"
    const val END_POINT = "https://babo.no/wp-json/"
    const val PREFS_NAME = "babo"
    const val LOG_TAG = "BABO"
    const val APP_FILES_DIR = "/.babo"
//    const val API_KEY = "bdae6825"
    const val API_KEY = "8201080a"

    // keys
    const val KEY_ACTIVE_USER = "active_user"
    const val KEY_EVENT = "event"
    const val KEY_EVENT_DETAILS = "event_details"
    const val KEY_LANGUAGE = "language"
    const val KEY_ATTENDEES = "attendees"
    const val KEY_IS_SOLD_SCREEN = "is_sold_screen"

    // req
    const val REQ_SCAN = 1
}