package no.babo.android.app

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric


class BaboApp : Application() {
    private var localizationDelegate = LocalizationApplicationDelegate(this)

    override fun onCreate() {
        super.onCreate()

        // init fabric
        Fabric.with(this, Crashlytics())
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localizationDelegate.onConfigurationChanged(this)
    }

    override fun getApplicationContext(): Context {
        return localizationDelegate.getApplicationContext(super.getApplicationContext())
    }
}