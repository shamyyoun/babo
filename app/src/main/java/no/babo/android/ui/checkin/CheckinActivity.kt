package no.babo.android.ui.checkin

import android.Manifest
import android.arch.lifecycle.Observer
import android.graphics.PointF
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.dlazaro66.qrcodereaderview.QRCodeReaderView
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_checkin.*
import no.babo.android.R
import no.babo.android.data.models.Event
import no.babo.android.ui.base.BaseActivity
import no.babo.android.ui.shared.views.PointsOverlayView
import no.babo.android.utils.Const
import no.babo.android.utils.InjectionUtils
import no.babo.android.utils.Utils

class CheckinActivity : BaseActivity(), QRCodeReaderView.OnQRCodeReadListener {
    private lateinit var viewModel: CheckinViewModel
    private lateinit var event: Event
    private var pointsView: PointsOverlayView? = null
    private var qrScanner: QRCodeReaderView? = null
    private lateinit var positiveSoundPlayer: MediaPlayer
    private lateinit var negativeSoundPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkin)

        title = ""
        enableBackButton()

        // check camera permission
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        initScannerView()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        // show toast and finis
                        Toast.makeText(this@CheckinActivity, R.string.you_must_grant_camera_permission_to_scan_qr_codes, Toast.LENGTH_LONG).show()
                        finish()
                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()

        getIntentData()

        // create media players
        positiveSoundPlayer = MediaPlayer.create(this, R.raw.positive_beep)
        negativeSoundPlayer = MediaPlayer.create(this, R.raw.negative_beep)
    }

    private fun initScannerView() {
        val layoutInflater = LayoutInflater.from(this)
        val layoutScannerContent = layoutInflater.inflate(R.layout.view_qr_scanner, layoutScanner, true)
        pointsView = layoutScannerContent.findViewById(R.id.pointsView)
        qrScanner = layoutScannerContent.findViewById(R.id.qrScanner)
        qrScanner?.setAutofocusInterval(500)
        qrScanner?.setOnQRCodeReadListener(this)
    }

    override fun onQRCodeRead(text: String, points: Array<PointF>) {
        // update points view
        pointsView?.setPoints(points)

        // get params
        val ticketId = Utils.getQueryValue(text, "ticket_id") ?: ""
        val securityCode = Utils.getQueryValue(text, "security_code") ?: ""

        viewModel.checkIn(ticketId, securityCode, event.id)

        enableScan(false)
    }

    override fun onResume() {
        super.onResume()
        qrScanner?.startCamera()
    }

    override fun onPause() {
        super.onPause()
        qrScanner?.stopCamera()
    }

    private fun getIntentData() {
        event = intent.getParcelableExtra(Const.KEY_EVENT)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // get the view model and start observing
        viewModel = getViewModel()

        observe()
    }

    private fun getViewModel() = InjectionUtils.provideCheckinViewModel(this)

    private fun observe() {
        viewModel.checkinEvent.observe(this, Observer {
            if (it == true) {
                updateUI(true)

                // play positive sound
                playBeep(true)
            }
        })

        viewModel.errorEvent.observe(this, Observer {
            if (it != null) {
                updateUI(false, it)

                // play negative sound
                playBeep(false)
            }
        })
    }

    private fun updateUI(result: Boolean, errorMsg: String? = null) {
        when (result) {
            true -> {
                ivBackground.setImageResource(R.drawable.green_vector_bg)
                ivEmoji.setImageResource(R.drawable.wink_face)
                tvResult.setText(R.string.checkin_successful)
                btnNext.setText(R.string.scan_next)
            }
            else -> {
                ivBackground.setImageResource(R.drawable.red_vector_bg)
                ivEmoji.setImageResource(R.drawable.killed_face)
                tvResult.text = errorMsg ?: getString(R.string.checkin_failed)
                btnNext.setText(R.string.try_again)
            }
        }
        btnNext.visibility = View.VISIBLE
    }

    public fun onNext(view: View) {
        enableScan()
    }

    private fun enableScan(enable: Boolean = true) {
        qrScanner?.setQRDecodingEnabled(enable)
        when (enable) {
            true -> {
                pointsView?.setPoints(arrayOf<PointF>())
                ivBackground.setImageResource(R.drawable.purple_vector_bg)
                ivEmoji.setImageResource(R.drawable.scan_face)
                tvResult.setText(R.string.scan_to_checkin)
                btnNext.visibility = View.GONE
            }
        }
    }

    private fun playBeep(positive: Boolean) {
        if (positive)
            positiveSoundPlayer.start()
        else
            negativeSoundPlayer.start()
    }

    override fun onDestroy() {
        positiveSoundPlayer.release()
        negativeSoundPlayer.release()
        super.onDestroy()
    }
}
