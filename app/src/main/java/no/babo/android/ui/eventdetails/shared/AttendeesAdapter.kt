package no.babo.android.ui.eventdetails.shared

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import no.babo.android.R
import no.babo.android.data.models.Attendant

class AttendeesAdapter(var data: List<Attendant>, private val onItemClickListener: (Attendant, Int) -> Unit)
    : RecyclerView.Adapter<AttendantViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttendantViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return AttendantViewHolder(layoutInflater.inflate(R.layout.item_attendant, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: AttendantViewHolder, position: Int) {
        with(data[position]) {
            holder.bind(this)
            holder.setOnItemClickListener { onItemClickListener.invoke(this, position) }
        }
    }
}