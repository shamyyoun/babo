package no.babo.android.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import kotlinx.android.synthetic.main.fragment_username_login.*
import no.babo.android.R

class UsernameLoginFragment : LoginFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_username_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // add listeners
        with(activity as LoginActivity) {
            etPassword.setOnEditorActionListener { view, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login(view)
                }
                true
            }
        }
    }

    internal fun getUsername() = etUsername.text.toString()

    internal fun getPassword() = etPassword.text.toString()
}
