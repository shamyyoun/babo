package no.babo.android.ui.login

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*
import no.babo.android.R
import no.babo.android.ui.base.BaseActivity
import no.babo.android.ui.events.EventsActivity
import no.babo.android.utils.InjectionUtils

class LoginActivity : BaseActivity() {
    private lateinit var viewModel: LoginViewModel
    private var usernameFragment: UsernameLoginFragment? = null
    private var apiFragment: ApiLoginFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // add listeners
        rgSwitchTab.setOnCheckedChangeListener { _, checkedId -> selectFragment(checkedId) }

        // load username login fragment
        selectFragment(R.id.rbUsername)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // get the view model and start observing
        viewModel = getViewModel()
        observe()
    }

    private fun getViewModel() = InjectionUtils.provideLoginViewModel(this)

    private fun observe() {
        // progress event
        viewModel.progressEvent.observe(this, Observer {
            when (it) {
                true -> showProgress()
                else -> hideProgress()
            }
        })

        // error event
        viewModel.errorEvent.observe(this, Observer {
            if (it != null) showShortToast(it)
        })

        // hide keyboard event
        viewModel.hideKeyboardEvent.observe(this, Observer {
            if (it == true) hideKeyboard()
        })

        // user event
        viewModel.userEvent.observe(this, Observer {
            // open events activity
            openEventsActivity()
        })
    }

    private fun selectFragment(id: Int) {
        val fragment = when (id) {
            R.id.rbUsername -> {
                usernameFragment = usernameFragment ?: UsernameLoginFragment()
                usernameFragment
            }

            else -> {
                apiFragment = apiFragment ?: ApiLoginFragment()
                apiFragment
            }
        }

        if (fragment != null) {
            loadFragment(fragment)
        }
    }

    fun login(view: View) {
        // check visible fragment
        if (usernameFragment?.isVisible == true) {
            val username = usernameFragment?.getUsername() ?: ""
            val password = usernameFragment?.getPassword() ?: ""
            viewModel.login(username, password)
        } else {
            val apiKey = apiFragment?.getApiKey() ?: ""
            viewModel.login(apiKey)
        }
    }

    private fun openEventsActivity() {
        startActivity(EventsActivity.newIntent(this))
        finish()
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }
}
