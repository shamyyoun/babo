package no.babo.android.ui.eventdetails.details

import android.os.Parcel
import android.os.Parcelable
import no.babo.android.data.models.Attendant

data class EventDetailsDataHolder(val id: Int,
                                  var attendees: List<Attendant>,
                                  val soldTicketsCount: Int,
                                  var checkInsCount: Int,
                                  val salesRevenue: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.createTypedArrayList(Attendant),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeTypedList(attendees)
        parcel.writeInt(soldTicketsCount)
        parcel.writeInt(checkInsCount)
        parcel.writeInt(salesRevenue)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EventDetailsDataHolder> {
        override fun createFromParcel(parcel: Parcel): EventDetailsDataHolder {
            return EventDetailsDataHolder(parcel)
        }

        override fun newArray(size: Int): Array<EventDetailsDataHolder?> {
            return arrayOfNulls(size)
        }
    }
}