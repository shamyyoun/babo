package no.babo.android.ui.base

import android.content.Context
import android.support.v7.app.AppCompatDialog
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import no.babo.android.R
import no.babo.android.utils.Utils

/**
 * Created by Shamyyoun on 2/17/2016.
 */
open class BaseDialog(context: Context) : AppCompatDialog(context) {
    private var rootView: FrameLayout? = null
    private var tvDialogTitle: TextView? = null
    private var ibClose: ImageButton? = null
    private var progressView: View? = null

    init {
        // set no title and transparent bg
        requestWindowFeature(Window.FEATURE_NO_TITLE)
    }

    override fun setContentView(layoutResId: Int) {
        super.setContentView(layoutResId)

        // init and customize the dialog toolbar
        rootView = findViewById(android.R.id.content)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        if (toolbar != null) {
            tvDialogTitle = findViewById(R.id.tvDialogTitle)
            ibClose = findViewById(R.id.ibClose)
            ibClose?.setOnClickListener { dismiss() }
        }
    }

    override fun setTitle(title: CharSequence?) {
        tvDialogTitle?.text = title
    }

    override fun setTitle(titleId: Int) = setTitle(getString(titleId))

    override fun setCancelable(flag: Boolean) {
        super.setCancelable(flag)
        ibClose?.visibility = if (flag) View.VISIBLE else View.GONE
    }

    protected fun logE(msg: String) = Utils.logE(msg)

    protected fun getString(strId: Int) = context.getString(strId)

    protected fun showProgress() {
        hideProgress()

        if (rootView != null && progressView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            progressView = inflater.inflate(R.layout.view_dialog_progress, null)
            rootView?.addView(progressView)
        }
        progressView?.visibility = View.VISIBLE
        super.setCancelable(false)
    }

    protected fun hideProgress() {
        progressView?.visibility = View.GONE
        super.setCancelable(true)
    }

    override fun dismiss() {
        // hide keyboard
        hideKeyboard()

        super.dismiss()
    }

    protected fun hideKeyboard() {
        if (rootView != null) {
            Utils.hideKeyboard(rootView!!)
        }
    }

    override fun show() {
        super.show()

        // customize the dialog width
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(window?.attributes)
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        window?.attributes = layoutParams
    }

    protected fun checkInternetConnection() = Utils.hasConnection(context)

    protected fun showShortToast(msg: String) = Utils.showShortToast(context, msg)

    protected fun showShortToast(msgId: Int) = Utils.showShortToast(context, msgId)

    protected fun showLongToast(msg: String) = Utils.showLongToast(context, msg)

    protected fun showLongToast(msgId: Int) = Utils.showLongToast(context, msgId)
}
