package no.babo.android.ui.checkin

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import no.babo.android.R
import no.babo.android.data.enums.Language
import no.babo.android.data.models.Attendant
import no.babo.android.data.repositories.TicketsRepository
import no.babo.android.ui.base.BaseViewModel
import no.babo.android.utils.DateUtils
import no.babo.android.utils.InjectionUtils
import no.babo.android.utils.Utils
import java.util.*

class CheckinViewModel(app: Application) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    private val ticketsRepository: TicketsRepository
    private val settingsRepository = InjectionUtils.provideSettingsRepository(app)
    val checkinEvent = MutableLiveData<Boolean>()
    private var auth = FirebaseAuth.getInstance()
    private var attendees: List<Attendant>? = null

    init {
        ticketsRepository = InjectionUtils.provideTicketsRepository(activeUserRepository.getUser()!!.token!!)
    }

    fun checkIn(ticketId: String, securityCode: String, eventId: Int) {
        // check authenticated firebase user
        if (auth.currentUser == null) {
            // sign in him
            auth.signInAnonymously()
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            checkUserInFirebase(eventId, securityCode, ticketId)
                        } else {
                            errorEvent.value = if (task.exception?.message != null) task.exception?.message else getString(R.string.error_authenticating_with_firebase)
                        }
                    }
        } else {
            checkUserInFirebase(eventId, securityCode, ticketId)
        }
    }

    private fun checkUserInFirebase(eventId: Int, securityCode: String, ticketId: String) {
        // check user status in firebase
        val database = FirebaseDatabase.getInstance()
        val dbRef = database.getReference("events/$eventId")
        dbRef.keepSynced(false)

        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                // check the status
                attendees = Gson().fromJson(p0.value as String, Array<Attendant>::class.java).toList()
                var foundUser = false
                for (item in attendees!!) {
                    if (item.securityCode == securityCode) {
                        if (item.checkedIn) {
                            // prepare error msg
                            var errorMsg: String
                            if (item.checkInDate != 0L) {
                                errorMsg = getString(R.string.already_checked_in)
                                Utils.logE("Date: " + item.checkInDate)
                                val checkInCalendar = DateUtils.convertToCalendar(item.checkInDate * 1000)
                                errorMsg += "\n" + if (DateUtils.isToday(checkInCalendar)) {
                                    Utils.logE("11111111")
                                    "${getString(R.string.at)} ${DateUtils.convertToString(checkInCalendar, "hh:mm a", getLocale())}"
                                } else {
                                    Utils.logE("2222222")
                                    "${getString(R.string.on)} ${DateUtils.convertToString(checkInCalendar, "E, d MMM", getLocale())}"
                                }
                            } else {
                                errorMsg = getString(R.string.already_n_checked_in)
                            }

                            // fire error event
                            errorEvent.value = errorMsg
                        } else {
                            item.checkedIn = true
                            item.checkInDate = DateUtils.getCurrentDateInSeconds()
                            updateFirebase(eventId, securityCode, ticketId)
                        }

                        foundUser = true
                        break
                    }
                }

                if (!foundUser) {
                    errorEvent.value = getString(R.string.failed_n_checking_in)
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                errorEvent.value = p0.message
            }
        })
    }

    private fun getLocale() = Locale(settingsRepository.getLanguage() ?: Language.ENGLISH.lang)

    private fun updateFirebase(eventId: Int, securityCode: String, ticketId: String) {
        val database = FirebaseDatabase.getInstance()
        val dbRef = database.getReference("events/$eventId")
        dbRef.keepSynced(false)

        dbRef.setValue(Gson().toJson(attendees))
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        checkinEvent.value = true
                        checkinServer(securityCode, ticketId, eventId)
                    } else {
                        errorEvent.value = if (task.exception?.message != null) task.exception?.message else getString(R.string.error_syncing_data_to_firebase)
                    }
                }
    }

    private fun checkinServer(securityCode: String, ticketId: String, eventId: Int) {
        ticketsRepository.checkin(securityCode, ticketId).observeForever {
            // check result
            if (it != true) {
                checkinServer(securityCode, ticketId, eventId)
            }
        }
    }
}