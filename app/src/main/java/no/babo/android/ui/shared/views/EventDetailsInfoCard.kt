package no.babo.android.ui.shared.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.com_event_details_info_card.view.*
import no.babo.android.R


class EventDetailsInfoCard : FrameLayout {

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet) {
        // get values
        val arr = context.obtainStyledAttributes(attrs, R.styleable.EventDetailsInfoCard)
        val icon = arr.getDrawable(R.styleable.EventDetailsInfoCard_edic_icon)
        val title = arr.getString(R.styleable.EventDetailsInfoCard_edic_title)
        val value = arr.getString(R.styleable.EventDetailsInfoCard_edic_value)

        // inflate root view
        LayoutInflater.from(context).inflate(R.layout.com_event_details_info_card, this)

        // set values
        ivIcon.setImageDrawable(icon)
        tvTitle.text = title
        tvValue.text = value


        // finish
        arr.recycle()
    }
}