package no.babo.android.ui.events

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_events.*
import no.babo.android.R
import no.babo.android.data.models.Event
import no.babo.android.ui.base.BaseActivity
import no.babo.android.ui.eventdetails.details.EventDetailsActivity
import no.babo.android.ui.settings.SettingsActivity
import no.babo.android.utils.InjectionUtils

class EventsActivity : BaseActivity() {
    private lateinit var viewModel: EventsViewModel
    private lateinit var adapter: EventsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events)
        setTitle(R.string.my_events)
        createOptionsMenu(R.menu.menu_main)

        // customize recycler view
        rvEvents.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        updateEventsAdapter()

        // customize swipe layout
        swipeLayout.setOnRefreshListener { viewModel.refresh() }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // get the view model and start observing
        viewModel = getViewModel()
        observe()

        // get events
        viewModel.fetchEvents()
    }

    private fun getViewModel() = InjectionUtils.provideEventsViewModel(this)

    private fun observe() {
        // progress event
        viewModel.progressEvent.observe(this, Observer {
            when (it) {
                true -> showProgress()
                else -> hideProgress()
            }
        })

        // error event
        viewModel.errorEvent.observe(this, Observer {
            swipeLayout.isRefreshing = false
            if (it != null) showShortToast(it)
        })

        // empty event
        viewModel.emptyEvent.observe(this, Observer {
            if (it == true) showShortToast(R.string.no_events_found)
        })

        // events event
        viewModel.eventsEvent.observe(this, Observer {
            if (it != null) {
                updateEventsAdapter(it)
            }
        })
    }

    private fun updateEventsAdapter(events: List<Event> = ArrayList()) {
        adapter = EventsAdapter(events, ::onEventClicked)
        rvEvents.adapter = adapter
    }

    private fun onEventClicked(event: Event) {
        openEventDetailsActivity(event)
    }

    private fun openEventDetailsActivity(event: Event) {
        startActivity(EventDetailsActivity.newIntent(this, event))
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, EventsActivity::class.java)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                // open settings activity
                startActivity(SettingsActivity.newIntent(this))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showProgress() {
        super.showProgress()
        swipeLayout.isRefreshing = false
    }

    override fun hideProgress(): Unit? {
        swipeLayout.isRefreshing = false
        return super.hideProgress()
    }
}
