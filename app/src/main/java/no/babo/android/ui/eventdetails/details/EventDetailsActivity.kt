package no.babo.android.ui.eventdetails.details

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.DividerItemDecoration
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_event_details.*
import kotlinx.android.synthetic.main.com_event_details_info_card.view.*
import no.babo.android.R
import no.babo.android.data.models.Attendant
import no.babo.android.data.models.Event
import no.babo.android.ui.base.BaseActivity
import no.babo.android.ui.checkin.CheckinActivity
import no.babo.android.ui.eventdetails.attendees.AttendeesActivity
import no.babo.android.ui.eventdetails.shared.AttendeesAdapter
import no.babo.android.ui.settings.SettingsActivity
import no.babo.android.utils.Const
import no.babo.android.utils.InjectionUtils

class EventDetailsActivity : BaseActivity() {
    private lateinit var viewModel: EventDetailsViewModel
    private lateinit var event: Event
    private lateinit var searchResultsAdapter: AttendeesAdapter
    private val searchHandler = Handler()
    private val searchRunnable = Runnable {
        search()
    }
    private var clickedAttendantPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)

        // init
        getIntentData()
        customizeToolbar()
        customizeSearchRV()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // get the view model and start observing
        viewModel = getViewModel()
        observe()

        // get details
        viewModel.fetchDetails(event.id)
    }

    private fun getIntentData() {
        event = intent.getParcelableExtra(Const.KEY_EVENT)
    }

    private fun customizeToolbar() {
        title = event.title
        enableBackButton()
        createOptionsMenu(R.menu.menu_event_details)
    }

    private fun customizeSearchRV() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                rvSearchResults.visibility = View.GONE
                searchHandler.removeCallbacks(searchRunnable)
                searchHandler.postDelayed(searchRunnable, 300)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        rvSearchResults.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        searchResultsAdapter = AttendeesAdapter(ArrayList(), ::onSearchResultItemClicked)
        rvSearchResults.adapter = searchResultsAdapter
    }

    private fun getViewModel() = InjectionUtils.provideEventDetailsViewModel(this)

    private fun observe() {
        // progress event
        viewModel.progressEvent.observe(this, Observer {
            when (it) {
                true -> showProgress()
                else -> hideProgress()
            }
        })

        // error event
        viewModel.errorEvent.observe(this, Observer {
            if (it != null) {
                showShortToast(it)
                enableControls(false)
            }
        })

        // details event
        viewModel.detailsEvent.observe(this, Observer {
            when (it) {
                null -> {
                    showShortToast(R.string.failed_loading_data)
                    enableControls(false)
                }
                else -> {
                    updateUI(it)
                    enableControls()
                }
            }
        })

        // checkin event
        viewModel.checkinEvent.observe(this, Observer {
            if (it == 0) {
                showShortToast(R.string.checkin_failed)
            } else {
                searchResultsAdapter.data[clickedAttendantPosition].checkedIn = it == 1
                showShortToast(if (it == 1) no.babo.android.R.string.checked_in_successfully else no.babo.android.R.string.checked_out_successfully)
                searchResultsAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun updateUI(details: EventDetailsDataHolder) {
        // set basic info
        infoCardSold.tvValue.text = details.soldTicketsCount.toString()
        infoCardCheckin.tvValue.text = details.checkInsCount.toString()
        infoCardSales.tvValue.text = getString(R.string.app_currency) + details.salesRevenue

        ibScan.setOnClickListener {
            onScanClicked()
        }

        if (rvSearchResults.visibility == View.VISIBLE) {
            search()
        }

    }

    private var searchTask: SearchTask? = null

    private fun search() {
        searchTask?.cancel(true)
        searchTask = SearchTask()
        searchTask?.execute()
    }

    inner class SearchTask : AsyncTask<Any?, Any?, Any?>() {
        override fun doInBackground(vararg params: Any?): Any? {
            val keyword = etSearch.text.toString()
            // check
            if (keyword.isNullOrEmpty() || viewModel?.detailsEvent.value?.attendees == null) {
                runOnUiThread { rvSearchResults.visibility = View.GONE }
                return null
            }

            // filter
            val filteredList = viewModel.detailsEvent.value?.attendees?.filter { it.title.toLowerCase().contains(keyword.toLowerCase()) }!!

            // check filtered list
            if (filteredList?.isEmpty()) {
                runOnUiThread { rvSearchResults.visibility = View.GONE }
            } else {
                runOnUiThread {
                    searchResultsAdapter.data = filteredList
                    searchResultsAdapter.notifyDataSetChanged()
                    rvSearchResults.visibility = View.VISIBLE
                }
            }

            return null
        }

    }

    private fun onSearchResultItemClicked(attendant: Attendant, position: Int) {
        this.clickedAttendantPosition = position

        with(attendant) {
            // show confirmation dialog
            val msg = getString(no.babo.android.R.string.do_you_want_to_x_y_manually_q,
                    getString(if (checkedIn) no.babo.android.R.string.uncheck else no.babo.android.R.string.check),
                    title)
            no.babo.android.utils.DialogUtils.showConfirmDialog(this@EventDetailsActivity, msg, android.content.DialogInterface.OnClickListener { dialog, _ ->
                hideKeyboard()

                // run check in request
                viewModel.checkIn(provider, this, event.id)

                dialog.dismiss()
            })
        }
    }

    private fun onScanClicked() {
        val intent = Intent(this, CheckinActivity::class.java)
        intent.putExtra(Const.KEY_EVENT_DETAILS, viewModel.detailsEvent.value)
        intent.putExtra(Const.KEY_EVENT, event)
        startActivityForResult(intent, Const.REQ_SCAN)
    }

    companion object {
        fun newIntent(context: Context, event: Event): Intent {
            val intent = Intent(context, EventDetailsActivity::class.java)
            intent.putExtra(Const.KEY_EVENT, event)
            return intent
        }
    }

    override fun onBackPressed() {
        if (rvSearchResults.visibility == View.VISIBLE) {
            rvSearchResults.visibility = View.GONE
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh -> {
                // refresh screen
                viewModel.refresh(event.id)
                true
            }
            R.id.action_settings -> {
                // open settings activity
                startActivity(SettingsActivity.newIntent(this))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun openCheckinAttendees(view: View) {
        startActivity(AttendeesActivity.newIntent(this, viewModel.detailsEvent.value!!, false))
    }

    fun openSoldTickets(view: View) {
        startActivity(AttendeesActivity.newIntent(this, viewModel.detailsEvent.value!!, true))
    }

    private fun enableControls(enable: Boolean = true) {
        ibScan.isClickable = enable
        infoCardCheckin.isClickable = enable
        infoCardSold.isClickable = enable
    }
}
