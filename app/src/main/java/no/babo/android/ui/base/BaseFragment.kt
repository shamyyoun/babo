package no.babo.android.ui.base

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import no.babo.android.R
import no.babo.android.utils.DialogUtils
import no.babo.android.utils.Utils

/**
 * Created by Shamyyoun on 5/28/16.
 */
open class BaseFragment : Fragment() {
    protected lateinit var activity: BaseActivity
    private var rootView: View? = null
    private var progressDialog: AlertDialog? = null

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        this.activity = activity as BaseActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity.hasToolbar()) {
            setHasOptionsMenu(true)
        }
    }

    protected fun findViewById(id: Int): View? = rootView?.findViewById(id)

    protected fun getResColor(id: Int) = resources.getColor(id)

    protected fun hideKeyboard() {
        if (rootView != null) {
            Utils.hideKeyboard(rootView!!)
        }
    }

    protected fun logE(msg: String) = Utils.logE(msg)

    protected fun loadFragment(fragment: Fragment, container: Int = R.id.container, enterAnim: Int = 0, exitAnim: Int = 0) =
            activity.loadFragment(fragment, container, enterAnim, exitAnim)

    protected fun showProgress() {
        when (progressDialog) {
            null -> progressDialog = DialogUtils.showProgressDialog(activity, R.string.please_wait_dotted)
            else -> if (!(progressDialog?.isShowing!!)) {
                progressDialog?.show()
            }
        }
    }

    protected fun hideProgress() = progressDialog?.dismiss()

    protected fun setTitle(title: CharSequence) {
        activity.title = title
    }

    protected fun setTitle(titleId: Int) = activity.setTitle(titleId)

    protected fun createOptionsMenu(menuId: Int) = activity.createOptionsMenu(menuId)

    protected fun removeOptionsMenu() = activity.removeOptionsMenu()

    protected fun checkInternetConnection() = Utils.hasConnection(activity)

    protected fun showShortToast(msg: String) = Utils.showShortToast(activity, msg)

    protected fun showShortToast(msgId: Int) = Utils.showShortToast(activity, msgId)

    protected fun showLongToast(msg: String) = Utils.showLongToast(activity, msg)

    protected fun showLongToast(msgId: Int) = Utils.showLongToast(activity, msgId)
}
