package no.babo.android.ui.events

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import no.babo.android.R
import no.babo.android.data.models.Event

class EventsAdapter(var data: List<Event>, private val onItemClickListener: (Event) -> Unit)
    : RecyclerView.Adapter<EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return EventViewHolder(layoutInflater.inflate(R.layout.item_event, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        with(data[position]) {
            holder.bind(this)
            holder.setOnItemClickListener { onItemClickListener.invoke(this) }
        }
    }
}