package no.babo.android.ui.eventdetails.attendees

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.DividerItemDecoration
import android.text.Editable
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_attendees.*
import no.babo.android.R
import no.babo.android.data.models.Attendant
import no.babo.android.ui.base.BaseActivity
import no.babo.android.ui.eventdetails.details.EventDetailsDataHolder
import no.babo.android.ui.eventdetails.shared.AttendeesAdapter
import no.babo.android.utils.Const
import no.babo.android.utils.InjectionUtils

class AttendeesActivity : BaseActivity() {
    private lateinit var viewModel: AttendeesViewModel
    private var isSoldScreen: Boolean = false
    private lateinit var eventDetails: EventDetailsDataHolder
    private lateinit var adapter: AttendeesAdapter
    private var clickedAttendantPosition = 0
    private val searchHandler = Handler()
    private val searchRunnable = Runnable {
        search()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendees)

        // init
        getIntentData()
        customizeToolbar()
        customizeSearchView()
        customizeRecyclerView()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // get the view model and start observing
        viewModel = getViewModel()
        observe()

        // fetch attendees
        viewModel.fetchAttendees()
    }

    private fun getIntentData() {
        isSoldScreen = intent.getBooleanExtra(Const.KEY_IS_SOLD_SCREEN, false)
        eventDetails = intent.getParcelableExtra(Const.KEY_EVENT_DETAILS)
    }

    private fun customizeToolbar() {
        setTitle(if (isSoldScreen) R.string.sold else R.string.check_in)
        setToolbarIcon(R.drawable.dark_gray_back_icon)
        enableBackButton()
    }

    private fun customizeSearchView() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                adapter.data = ArrayList()
                adapter.notifyDataSetChanged()
                searchHandler.removeCallbacks(searchRunnable)
                searchHandler.postDelayed(searchRunnable, 300)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun customizeRecyclerView() {
        rvAttendees.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        adapter = AttendeesAdapter(ArrayList(), ::onItemClicked)
        rvAttendees.adapter = adapter
    }

    private fun getViewModel() = InjectionUtils.provideAttendeesViewModel(this, eventDetails, isSoldScreen)

    private fun observe() {
        // progress event
        viewModel.progressEvent.observe(this, Observer {
            when (it) {
                true -> showProgress()
                else -> hideProgress()
            }
        })

        // error event
        viewModel.errorEvent.observe(this, Observer {
            if (it != null) showShortToast(it)
        })

        // attendees event
        viewModel.attendeesEvent.observe(this, Observer {
            when (it) {
                null -> showShortToast(R.string.failed_loading_data)
                else -> if (it.isEmpty()) {
                    showShortToast(if (isSoldScreen) R.string.no_sold_tickets_yet else R.string.no_check_in_yet)
                } else {
                    updateUI(it)
                }
            }
        })

        // checkin event
        viewModel.checkinEvent.observe(this, Observer {
            if (it == 0) {
                showShortToast(R.string.checkin_failed)
            } else {
                showShortToast(if (it == 1) no.babo.android.R.string.checked_in_successfully else no.babo.android.R.string.checked_out_successfully)
                updateUI(viewModel.attendeesEvent.value!!)
            }
        })
    }

    private fun updateUI(attendees: List<Attendant>) {
        adapter.data = attendees
        adapter.notifyDataSetChanged()

        if (etSearch.text.isNotEmpty()) {
            search()
        }
    }

    private fun onItemClicked(attendant: Attendant, position: Int) {
        this.clickedAttendantPosition = position

        with(attendant) {
            // show confirmation dialog
            val msg = getString(no.babo.android.R.string.do_you_want_to_x_y_manually_q,
                    getString(if (checkedIn) no.babo.android.R.string.uncheck else no.babo.android.R.string.check),
                    title)
            no.babo.android.utils.DialogUtils.showConfirmDialog(this@AttendeesActivity, msg, android.content.DialogInterface.OnClickListener { dialog, _ ->
                hideKeyboard()

                // run check in request
                viewModel.checkIn(provider, this)

                dialog.dismiss()
            })
        }
    }

    private var searchTask: SearchTask? = null

    private fun search() {
        searchTask?.cancel(true)
        searchTask = SearchTask()
        searchTask?.execute()
    }

    inner class SearchTask : AsyncTask<Any?, Any?, Any?>() {
        override fun doInBackground(vararg params: Any?): Any? {
            val keyword = etSearch.text.toString()
            // check
            if (keyword.isNullOrEmpty()) {
                runOnUiThread {
                    adapter.data = viewModel?.attendeesEvent.value!!
                    adapter.notifyDataSetChanged()
                }
                return null
            }

            // filter
            val filteredList = viewModel.attendeesEvent.value?.filter { it.title.toLowerCase().contains(keyword.toLowerCase()) }!!

            runOnUiThread {
                adapter.data = filteredList
                adapter.notifyDataSetChanged()
            }

            return null
        }

    }

    companion object {
        fun newIntent(context: Context, eventDetails: EventDetailsDataHolder, isSoldScreen: Boolean): Intent {
            val intent = Intent(context, AttendeesActivity::class.java)
            intent.putExtra(Const.KEY_EVENT_DETAILS, eventDetails)
            intent.putExtra(Const.KEY_IS_SOLD_SCREEN, isSoldScreen)
            return intent
        }
    }
}
