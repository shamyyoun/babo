package no.babo.android.ui.eventdetails.details

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import no.babo.android.R
import no.babo.android.data.models.Attendant
import no.babo.android.data.models.Ticket
import no.babo.android.data.repositories.TicketsRepository
import no.babo.android.ui.base.BaseViewModel
import no.babo.android.utils.DateUtils
import no.babo.android.utils.InjectionUtils


class EventDetailsViewModel(app: Application) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    private val ticketsRepository: TicketsRepository
    val detailsEvent = MutableLiveData<EventDetailsDataHolder>()
    val checkinEvent = MutableLiveData<Int>()
    private var auth = FirebaseAuth.getInstance()
    private var attendant: Attendant? = null

    init {
        ticketsRepository = InjectionUtils.provideTicketsRepository(activeUserRepository.getUser()!!.token!!)
    }

    fun fetchDetails(eventId: Int) {
        if (!checkInternetConnection()) {
            // trigger error event
            progressEvent.value = false
            errorEvent.value = getString(R.string.no_internet_connection)
        } else {
            processFetchTickets(eventId)
        }
    }

    private fun processFetchTickets(eventId: Int) {
        progressEvent.value = true

        ticketsRepository.tickets(eventId).observeForever {
            // check user
            if (it == null) {
                // trigger hiding progress
                progressEvent.value = false
                // trigger error event
                errorEvent.value = getString(R.string.error_loading_event_details)
            } else when (it) {
                null -> {
                    // trigger hiding progress
                    progressEvent.value = false
                    errorEvent.value = getString(R.string.error_loading_event_details)
                }
                else -> {
                    // get event details object
                    val details = getDataHolder(eventId, it)

                    // check authenticated firebase user
                    if (auth.currentUser == null) {
                        // sign in him
                        auth.signInAnonymously()
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        // update firebase
                                        syncDataWithFirebase(eventId, details)
                                    } else {
                                        // trigger hiding progress
                                        progressEvent.value = false
                                        errorEvent.value = if (task.exception?.message != null) task.exception?.message else getString(R.string.error_authenticating_with_firebase)
                                    }
                                }
                    } else {
                        // update firebase
                        syncDataWithFirebase(eventId, details)
                    }
                }
            }
        }
    }

    private fun syncDataWithFirebase(eventId: Int, details: EventDetailsDataHolder) {
        val database = FirebaseDatabase.getInstance()
        val dbRef = database.getReference("events/$eventId")
        dbRef.keepSynced(false)

        // update attendees from details obj
        dbRef.setValue(Gson().toJson(details.attendees))
                .addOnCompleteListener { task ->
                    progressEvent.value = false

                    if (task.isSuccessful) {
                        detailsEvent.value = details
                    } else {
                        progressEvent.value = false
                        errorEvent.value = if (task.exception?.message != null) task.exception?.message else getString(R.string.error_syncing_data_to_firebase)
                    }
                }

        // start listening for changes
        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                try {
                    val attendees: List<Attendant> = Gson().fromJson(p0.value as String, Array<Attendant>::class.java).toList()
                    details.attendees = attendees
                    updateCheckinCount()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                logE("Cancelled: ${p0.message}")
            }
        })
    }

    private fun getDataHolder(eventId: Int, tickets: List<Ticket>): EventDetailsDataHolder {
        val attendees = ArrayList<Attendant>()
        var soldTicketsCount = 0
        var checkInsCount = 0
        var salesRevenue = 0

        for (ticket in tickets) {
            // add to attendees
            attendees.addAll(ticket.attendees)

            // calc sold tickets count
            soldTicketsCount += ticket.attendees.size

            // calc checkins count and sales revenue
            for (attendant in ticket.attendees) {
                with(attendant) {
                    if (checkedIn) checkInsCount++
                    salesRevenue += paidAmount
                }
            }
        }

        // return with data holder obj
        return EventDetailsDataHolder(eventId, attendees, soldTicketsCount, checkInsCount, salesRevenue)
    }

    fun checkIn(provider: String, attendant: Attendant, eventId: Int) {
        if (!checkInternetConnection()) {
            // trigger error event
            progressEvent.value = false
            errorEvent.value = getString(R.string.no_internet_connection)
        } else {
            progressEvent.value = true

            this.attendant = attendant
            ticketsRepository.checkin(provider, attendant.id, eventId).observeForever {
                if (it == true) {
                    with(attendant) {
                        val newStatus = !checkedIn

                        // update this attendee in the main list
                        for (attendant in detailsEvent.value!!.attendees) {
                            if (id == attendant.id) {
                                attendant.checkedIn = newStatus
                                attendant.checkInDate = if (newStatus) DateUtils.getCurrentDateInSeconds() else 0
                                checkedIn = newStatus
                                break
                            }
                        }

                        updateFirebase(eventId, if (newStatus) 1 else 2)
                    }
                } else {
                    progressEvent.value = false
                    errorEvent.value = getString(R.string.failed_checking_in)
                }
            }
        }
    }

    private fun updateFirebase(eventId: Int, checked: Int) {
        val details = detailsEvent.value
        if (details != null) {
            val database = FirebaseDatabase.getInstance()
            val dbRef = database.getReference("events/$eventId")
            dbRef.keepSynced(false)

            // update attendees from details obj
            dbRef.setValue(Gson().toJson(details.attendees))
                    .addOnCompleteListener { task ->
                        progressEvent.value = false
                        if (task.isSuccessful) {
                            checkinEvent.value = checked
                        } else {
                            progressEvent.value = false
                            errorEvent.value = if (task.exception?.message != null) task.exception?.message else getString(R.string.error_syncing_data_to_firebase)
                        }
                    }
        }
    }

    fun updateCheckinCount() {
        var checkInsCount = 0

        // calc checkins count and sales revenue
        for (attendant in detailsEvent.value?.attendees!!) {
            with(attendant) {
                if (checkedIn) checkInsCount++
            }
        }

        val detailsObj = detailsEvent.value
        detailsObj?.checkInsCount = checkInsCount
        detailsEvent.value = detailsObj
    }

    fun refresh(eventId: Int) = fetchDetails(eventId)
}