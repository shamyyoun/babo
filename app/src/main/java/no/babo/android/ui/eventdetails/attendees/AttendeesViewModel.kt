package no.babo.android.ui.eventdetails.attendees

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import no.babo.android.R
import no.babo.android.data.models.Attendant
import no.babo.android.data.repositories.TicketsRepository
import no.babo.android.ui.base.BaseViewModel
import no.babo.android.ui.eventdetails.details.EventDetailsDataHolder
import no.babo.android.utils.DateUtils
import no.babo.android.utils.InjectionUtils


class AttendeesViewModel(app: Application, private val eventDetails: EventDetailsDataHolder,
                         private val isSoldScreen: Boolean) : BaseViewModel(app) {

    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    private val ticketsRepository: TicketsRepository
    val attendeesEvent = MutableLiveData<List<Attendant>>()
    val checkinEvent = MutableLiveData<Int>()
    private var auth = FirebaseAuth.getInstance()
    private var attendant: Attendant? = null

    init {
        ticketsRepository = InjectionUtils.provideTicketsRepository(activeUserRepository.getUser()!!.token!!)
    }

    fun fetchAttendees() {
        filterAttendees()

        preStartListeningForChangesFromFirebase()
    }

    private fun filterAttendees() {
        if (isSoldScreen) {
            attendeesEvent.value = eventDetails.attendees
        } else {
            val attendees = ArrayList<Attendant>()
            for (attendant in eventDetails.attendees) {
                if (attendant.checkedIn) attendees.add(attendant)
            }
            attendeesEvent.value = attendees
        }
    }

    private fun preStartListeningForChangesFromFirebase() {
        // check authenticated firebase user
        if (auth.currentUser == null) {
            // sign in him
            auth.signInAnonymously()
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            startListeningForChangesFromFirebase()
                        } else {
                            // trigger hiding progress
                            progressEvent.value = false
                            errorEvent.value = if (task.exception?.message != null) task.exception?.message else getString(R.string.error_authenticating_with_firebase)
                        }
                    }
        } else {
            startListeningForChangesFromFirebase()
        }
    }

    private fun startListeningForChangesFromFirebase() {
        val database = FirebaseDatabase.getInstance()
        val dbRef = database.getReference("events/${eventDetails.id}")
        dbRef.keepSynced(false)

        // start listening for changes
        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                try {
                    val attendees: List<Attendant> = Gson().fromJson(p0.value as String, Array<Attendant>::class.java).toList()
                    eventDetails.attendees = attendees
                    filterAttendees()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                logE("Cancelled: ${p0.message}")
            }
        })
    }

    fun checkIn(provider: String, attendant: Attendant) {
        if (!checkInternetConnection()) {
            // trigger error event
            progressEvent.value = false
            errorEvent.value = getString(R.string.no_internet_connection)
        } else {
            progressEvent.value = true

            this.attendant = attendant
            ticketsRepository.checkin(provider, attendant.id, eventDetails.id).observeForever {
                if (it == true) {
                    with(attendant) {
                        val newStatus = !checkedIn

                        // update or remove this attendee in attendees list
                        for (attendant in attendeesEvent.value!!) {
                            if (id == attendant.id) {
                                attendant.checkedIn = newStatus
                                attendant.checkInDate = if (newStatus) DateUtils.getCurrentDateInSeconds() else 0
                                checkedIn = newStatus
                                break
                            }
                        }

                        // update or remove this attendee in event details attendees list
                        for (attendant in eventDetails.attendees) {
                            if (id == attendant.id) {
                                attendant.checkedIn = newStatus
                                attendant.checkInDate = if (newStatus) DateUtils.getCurrentDateInSeconds() else 0
                                checkedIn = newStatus
                                break
                            }
                        }

                        preUpdateFirebase(if (newStatus) 1 else 2)
                    }
                } else {
                    progressEvent.value = false
                    errorEvent.value = getString(R.string.failed_checking_in)
                }
            }
        }
    }

    private fun preUpdateFirebase(checked: Int) {
        if (attendeesEvent.value != null) {

            // check authenticated firebase user
            if (auth.currentUser == null) {
                // sign in him
                auth.signInAnonymously()
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                // update firebase
                                updateFirebase(checked)
                            } else {
                                // trigger hiding progress
                                progressEvent.value = false
                                errorEvent.value = if (task.exception?.message != null) task.exception?.message else getString(R.string.error_authenticating_with_firebase)
                            }
                        }
            } else {
                updateFirebase(checked)
            }
        }
    }

    private fun updateFirebase(checked: Int) {
        val database = FirebaseDatabase.getInstance()
        val dbRef = database.getReference("events/${eventDetails.id}")
        dbRef.keepSynced(false)

        // update attendees from details obj
        dbRef.setValue(Gson().toJson(eventDetails.attendees))
                .addOnCompleteListener { task ->
                    progressEvent.value = false
                    if (task.isSuccessful) {
                        checkinEvent.value = checked
                    } else {
                        progressEvent.value = false
                        errorEvent.value = if (task.exception?.message != null) task.exception?.message else getString(R.string.error_syncing_data_to_firebase)
                    }
                }
    }
}