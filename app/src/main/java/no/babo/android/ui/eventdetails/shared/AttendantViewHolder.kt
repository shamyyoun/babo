package no.babo.android.ui.eventdetails.shared

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import no.babo.android.R
import no.babo.android.data.models.Attendant
import no.babo.android.ui.base.BaseRecyclerViewHolder

class AttendantViewHolder(rootView: View) : BaseRecyclerViewHolder(rootView) {
    private lateinit var attendant: Attendant
    val tvTitle = findViewById(R.id.tvTitle) as TextView
    val tvTicketId = findViewById(R.id.tvTicketId) as TextView
    val ivCheckedin = findViewById(R.id.ivCheckedin) as ImageView

    fun bind(attendant: Attendant) {
        with(attendant) {
            this@AttendantViewHolder.attendant = this
            tvTitle.text = title
            tvTicketId.text = uniqueId
            ivCheckedin.setImageResource(if (checkedIn) no.babo.android.R.drawable.colored_checkid_in_icon else no.babo.android.R.drawable.gray_checkedin_icon)
        }
    }
}