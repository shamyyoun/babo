package no.babo.android.ui.splash

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_splash.*
import no.babo.android.R
import no.babo.android.ui.base.BaseActivity
import no.babo.android.ui.events.EventsActivity
import no.babo.android.ui.login.LoginActivity
import no.babo.android.utils.InjectionUtils

class SplashActivity : BaseActivity() {
    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // get the view model and start observing
        viewModel = getViewModel()
        observe()

        // start splash
        viewModel.startSplash()
    }

    private fun getViewModel() = InjectionUtils.provideSplashViewModel(this)

    private fun observe() {
        // progress event
        viewModel.progressEvent.observe(this, Observer {
            when (it) {
                true -> showProgress()
                else -> hideProgress()
            }
        })

        // error event
        viewModel.errorEvent.observe(this, Observer {
            if (it != null) showShortToast(it)
        })

        // user event
        viewModel.hasUserEvent.observe(this, Observer {
            when (it) {
                true -> openEventsActivity()
                else -> openLoginActivity()
            }
        })
    }

    private fun openLoginActivity() {
        startActivity(LoginActivity.newIntent(this))
        finish()
    }

    private fun openEventsActivity() {
        startActivity(EventsActivity.newIntent(this))
        finish()
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, SplashActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    or Intent.FLAG_ACTIVITY_NEW_TASK)
            return intent
        }
    }
}
