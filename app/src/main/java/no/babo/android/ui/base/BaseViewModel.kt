package no.babo.android.ui.base

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import no.babo.android.utils.Utils

open class BaseViewModel(protected val app: Application) : AndroidViewModel(app) {
    val progressEvent = MutableLiveData<Boolean>()
    val errorEvent = MutableLiveData<String>()
    val hideKeyboardEvent = MutableLiveData<Boolean>()

    protected fun logE(msg: String) = Utils.logE(msg)

    protected fun checkInternetConnection() = Utils.hasConnection(app)

    protected fun getString(strId: Int) = app.applicationContext.getString(strId)
}