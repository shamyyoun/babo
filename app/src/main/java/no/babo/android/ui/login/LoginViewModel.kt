package no.babo.android.ui.login

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import no.babo.android.R
import no.babo.android.data.models.User
import no.babo.android.data.repositories.AuthenticationRepository
import no.babo.android.ui.base.BaseViewModel
import no.babo.android.utils.InjectionUtils

class LoginViewModel(app: Application, private val userRepository: AuthenticationRepository) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    val userEvent = MutableLiveData<User>()

    fun login(username: String, password: String) {
        if (!checkCredentials(username, password)) {
            // trigger error event
            errorEvent.value = getString(R.string.username_and_password_are_required)
        } else if (!checkInternetConnection()) {
            // trigger events
            hideKeyboardEvent.value = true
            errorEvent.value = getString(R.string.no_internet_connection)
        } else {
            processLogin(username, password)
        }
    }

    fun login(apiKey: String) {
        if (!checkCredentials(apiKey)) {
            // trigger error event
            errorEvent.value = getString(R.string.api_key_is_required)
        } else if (!checkInternetConnection()) {
            // trigger events
            hideKeyboardEvent.value = true
            errorEvent.value = getString(R.string.no_internet_connection)
        } else {
            processLogin(apiKey = apiKey)
        }
    }

    private fun checkCredentials(username: String, password: String) =
            !username.isEmpty() && !password.isEmpty()

    private fun checkCredentials(apiKey: String) = !apiKey.isEmpty()

    private fun processLogin(username: String = "", password: String = "", apiKey: String = "") {
        // trigger events
        hideKeyboardEvent.value = true
        progressEvent.value = true

        userRepository.login(username, password, apiKey).observeForever {
            // trigger hiding progress
            progressEvent.value = false

            // check user
            if (it == null) {
                // trigger error event
                errorEvent.value = getString(R.string.login_failed)
            } else {
                // update the active event id if the user is logged in with apiKey
                if (apiKey.isNotEmpty() && it.events.isNotEmpty()) {
                    it.activeEventId = it.events[0].id
                }

                // update the active user
                activeUserRepository.updateUser(it)

                // trigger the user event
                userEvent.value = it
            }
        }
    }
}