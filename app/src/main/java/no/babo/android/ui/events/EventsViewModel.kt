package no.babo.android.ui.events

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import no.babo.android.R
import no.babo.android.data.models.Event
import no.babo.android.data.repositories.TicketsRepository
import no.babo.android.ui.base.BaseViewModel
import no.babo.android.utils.InjectionUtils

class EventsViewModel(app: Application) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    private val ticketsRepository: TicketsRepository
    val eventsEvent = MutableLiveData<List<Event>>()
    val emptyEvent = MutableLiveData<Boolean>()

    init {
        ticketsRepository = InjectionUtils.provideTicketsRepository(activeUserRepository.getUser()!!.token!!)
    }

    fun fetchEvents() {
        if (!checkInternetConnection()) {
            // trigger error event
            errorEvent.value = getString(R.string.no_internet_connection)
        } else {
            processFetchEvents()
        }
    }

    private fun processFetchEvents() {
        progressEvent.value = true

        ticketsRepository.myEvents().observeForever { events ->
            // trigger hiding progress
            progressEvent.value = false

            // check events
            when {
                events == null -> errorEvent.value = getString(R.string.error_loading_events)
                events.isEmpty() -> emptyEvent.value = true
                else -> {
                    val activeEventId = activeUserRepository.getUser()?.activeEventId ?: 0
                    if (activeEventId != 0) {
                        val activeEvent = events.filter { it.id == activeEventId }
                        eventsEvent.value = activeEvent
                    } else {
                        eventsEvent.value = events
                    }
                }
            }
        }
    }

    fun refresh() = fetchEvents()
}