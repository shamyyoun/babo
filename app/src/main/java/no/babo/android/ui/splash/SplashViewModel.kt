package no.babo.android.ui.splash

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.os.Handler
import no.babo.android.data.repositories.AuthenticationRepository
import no.babo.android.ui.base.BaseViewModel
import no.babo.android.utils.InjectionUtils

class SplashViewModel(app: Application, private val userRepository: AuthenticationRepository) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    val hasUserEvent = MutableLiveData<Boolean>()

    fun startSplash() {
        // check if there's an active user
        if (activeUserRepository.hasActiveUser()) {
            // trigger has user event
            hasUserEvent.value = true
        } else {
            processSplash()
        }
    }

    private fun processSplash() {
        Handler().postDelayed({
            // trigger has user event
            hasUserEvent.value = false
        }, SPLASH_DELAY)
    }

    companion object {
        private const val SPLASH_DELAY = 2000L
    }
}