package no.babo.android.ui.settings

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import no.babo.android.data.enums.Language
import no.babo.android.ui.base.BaseViewModel
import no.babo.android.utils.InjectionUtils


class SettingsViewModel(app: Application) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    private val settingsRepository = InjectionUtils.provideSettingsRepository(app)
    val fetchLanguageEvent = MutableLiveData<Language>()
    val logoutEvent = MutableLiveData<Boolean>()
    val changeLanguageEvent = MutableLiveData<Language>()

    fun fetchLanguage() {
        fetchLanguageEvent.value = when (settingsRepository.getLanguage()) {
            Language.NORWEGIAN.lang -> Language.NORWEGIAN
            else -> Language.ENGLISH
        }
    }

    fun logout() {
        activeUserRepository.removeUser()
        logoutEvent.value = true
    }

    fun changeLanguage(language: Language) {
        // save new language
        settingsRepository.updateLanguage(language.lang)

        // fire event
        changeLanguageEvent.value = language
    }
}