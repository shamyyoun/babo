package no.babo.android.ui.events

import android.view.View
import android.widget.TextView
import no.babo.android.R
import no.babo.android.data.models.Event
import no.babo.android.ui.base.BaseRecyclerViewHolder
import no.babo.android.utils.DateUtils

class EventViewHolder(rootView: View) : BaseRecyclerViewHolder(rootView) {
    private lateinit var event: Event
    private val tvTitle = findViewById(R.id.tvTitle) as TextView
    private val tvDate = findViewById(R.id.tvDate) as TextView

    fun bind(event: Event) {
        with(event) {
            this@EventViewHolder.event = this
            tvTitle.text = title
            tvDate.text = DateUtils.formatDate(startDate, DateUtils.SERVER_DATE_TIME_FORMAT, DISPLAYED_DATE_FORMAT)
        }
    }

    companion object {
        private const val DISPLAYED_DATE_FORMAT = "yyyy-M-d hh:mm a"
    }
}