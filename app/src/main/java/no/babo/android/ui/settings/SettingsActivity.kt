package no.babo.android.ui.settings

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import kotlinx.android.synthetic.main.activity_settings.*
import no.babo.android.R
import no.babo.android.data.enums.Language
import no.babo.android.ui.base.BaseActivity
import no.babo.android.ui.splash.SplashActivity
import no.babo.android.utils.DialogUtils
import no.babo.android.utils.InjectionUtils

class SettingsActivity : BaseActivity() {
    private lateinit var viewModel: SettingsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        // init
        customizeToolbar()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // get the view model and start observing
        viewModel = getViewModel()
        observe()

        // fetch language
        viewModel.fetchLanguage()
    }

    private fun customizeToolbar() {
        setToolbarIcon(R.drawable.dark_gray_back_icon)
        setTitle(R.string.settings)
        enableBackButton()
    }

    private fun addLanguageCheckChangeListener() {
        rgLanguage.setOnCheckedChangeListener(languageRGCheckChangeListener)
    }

    private val languageRGCheckChangeListener = RadioGroup.OnCheckedChangeListener { _, checkedId ->
        when (checkedId) {
            R.id.rbEnglish -> viewModel.changeLanguage(Language.ENGLISH)
            R.id.rbNorwegian -> viewModel.changeLanguage(Language.NORWEGIAN)
        }
    }

    private fun getViewModel() = InjectionUtils.provideSettingsViewModel(this)

    private fun observe() {
        // fetch language event
        viewModel.fetchLanguageEvent.observe(this, Observer {
            if (it?.lang == Language.NORWEGIAN.lang)
                rgLanguage.check(R.id.rbNorwegian)
            else
                rgLanguage.check(R.id.rbEnglish)
            addLanguageCheckChangeListener()
        })

        // change language event
        viewModel.changeLanguageEvent.observe(this, Observer {
            if (it != null) {
                // set the new language
                setLanguage(it.lang)
            }
        })

        // logout event
        viewModel.logoutEvent.observe(this, Observer {
            if (it == true) {
                // open splash
                startActivity(SplashActivity.newIntent(this))
            }
        })
    }

    fun onLogout(v: View) {
        // show confirmation dialog
        DialogUtils.showConfirmDialog(this, R.string.do_you_want_to_logout_q, DialogInterface.OnClickListener { _, _ ->
            viewModel.logout()
        })
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SettingsActivity::class.java)
        }
    }
}
